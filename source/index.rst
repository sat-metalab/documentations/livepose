LivePose
========

.. warning::

   We are in the process of improving the documentation. You can test it `here <contents.html>`__, and you are very welcome to signal issues with the documentation `on the issue tracker <https://gitlab.com/sat-metalab/documentations/livepose/-/issues>`__

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   welcome to the LivePose documentation website
  
.. container:: index-menu

   `-` `documentation <contents.html>`__ - `gitlab <https://gitlab.com/sat-metalab/livepose>`__- `about us <https://sat.qc.ca/fr/recherche/metalab>`__ - `get in touch <https://gitlab.com/sat-metalab/livepose/-/issues>`__ -
  

.. container:: index-section

    what is LivePose?

LivePose is a command line tool which tracks people skeletons from a RGB or grayscale video feed (live or not), applies various filters on them (for detection, selection, improving the data, etc) and sends the results through the network (OSC and Websockets are currently supported). 

LivePose is able to do all of this in real-time, processing *live* video streams and sending out results for each frame at 20-30 FPS.

As some example of what the software can do, there are filters to detect specific actions (jumping, raising arms), detecting skeletons position onto a plane, controlling the avatar in `Mozilla Hubs <https://hubs.mozilla.org>`__, etc. There are some ongoing work to add support for 3D skeleton detection using multiple cameras, and for more robust action detection using a neural network.
  
.. raw:: html

  <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/604196712?color=8cc747" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

    
.. container:: index-section

   why LivePose?

This software is meant to give access to the impressive work around skeleton detection using neural networks in the recent years. These new methods are often hard to use and deploy, and as a lot of code and libraries created throughout research projects they are not always well maintained throughout time. Also in some cases the licenses prevent using them easily in commercial work, which can includes artistic pieces. On top of that, deep neural network frameworks have their own issues regarding deployment and maintainability.

LivePose is focused on implementing algorithms around hardened libraries. For example the PoseNet implementation uses the OpenCV deep neural network module (DNN). OpenCV has proven to be a stable and well-maintained library which will hopefully allow for this implementation to work for many years. And OpenCV supports a lot of acceleration backends for DNN.
  
.. .. container:: index-section

..    table of contents
   
.. * how to `install LivePose <./install/contents.html>`__
.. * `a first tutorial <./tutorials/first_steps.html#standard-usages>`__ to get you started
.. * `tutorials <./tutorials/first_steps.html#advanced-usages>`__ to grow your skills
.. * `how-to guides <./howto/contents.html>`__ for step-by-step instructions to accomplish specific tasks
.. * `Frequently Asked Questions (FAQ) <./faq/contents.html>`__ for your troubleshooting needs

  
.. container:: index-section

   please show me the code!
   
For more information visit `the code repository <https://gitlab.com/sat-metalab/livepose>`__.
  
.. container:: index-section

   sponsors

This project is made possible thanks to the `Society for Arts and Technologies <http://www.sat.qc.ca>`__ (also known as SAT).


