Installing LivePose by Compiling Dependencies from Source
=========================================================

These are instructions for installing LivePose by building all of its dependencies from source. This may be useful if you want to use custom versions of CUDA and CuDNN.

Clone The LivePose Repo
-----------------------

1. Clone the LivePose repo

.. code:: bash

   sudo apt install git
   git clone https://gitlab.com/sat-mtl/tools/livepose
   cd livepose

2. Install and setup Git LFS

.. code:: bash

   sudo apt install git-lfs
   git lfs fetch && git lfs pull

Install Dependencies
--------------------

Install Main Dependencies with apt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install all system dependencies

.. code:: bash

   sudo apt install build-essential python3-dev python3-pip python3-venv python3-wheel

Install CUDA and CuDNN
~~~~~~~~~~~~~~~~~~~~~~

**Note**: These instructions assume you're using Ubuntu 22.04 or 20.04. If you're using a different distribution or version, refer to the official NVIDIA Instructions for `CUDA <https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html>`__ and `CuDNN <https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html>`__, respectively.

1. Install / Update NVIDIA drivers

Find recommended drivers for your setup:

.. code:: bash

   ubuntu-drivers devices | grep recommended

Example output:

.. code:: bash

   driver   : nvidia-driver-470 - third-party non-free recommended

Install the recommended driver (replace ``470`` with the version recommended on your system):

.. code:: bash

   sudo apt install nvidia-driver-470

2. Install Dependencies

.. code:: bash

   sudo apt install wget software-properties-common gnupg

3. Setup Nvidia CUDA Repository

.. code:: bash

   OS="ubuntu`lsb_release -rs | sed 's/\.//'`"
   wget https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/cuda-${OS}.pin
   sudo mv cuda-${OS}.pin /etc/apt/preferences.d/cuda-repository-pin-600
   #sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/7fa2af80.pub
   sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/3bf863cc.pub
   sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/${OS}/x86_64/ /"
   sudo apt update

4. Install CUDA

.. code:: bash

   sudo apt install cuda

Update your ``PATH`` to include CUDA binaries:

.. code:: bash

   export PATH=/usr/local/cuda-11.4/bin${PATH:+:${PATH}}

You can update your ``.bashrc`` so that ``PATH`` will always be set to include CUDA binaries:

.. code:: bash

   echo 'export PATH=/usr/local/cuda/bin${PATH:+:${PATH}}' >> ~/.bashrc

You can verify the CUDA installation and version with:

.. code:: bash

   nvcc --version

Example output:

.. code:: bash

   nvcc: NVIDIA (R) Cuda compiler driver
   Copyright (c) 2005-2019 NVIDIA Corporation
   Built on Sun_Jul_28_19:07:16_PDT_2019
   Cuda compilation tools, release 10.1, V10.1.243

5. Install CuDNN

.. code:: bash

   sudo apt install libcudnn8 libcudnn8-dev

Create a Virtual Environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LivePose has Python dependencies installed through apt and pip. We create a virtual environment so that LivePose can access dependencies from both sources.

1. Create a virtual environment (you can name this whatever you like)

.. code:: bash

   python3 -m venv --system-site-packages --symlinks ~/livepose_venv

2. Activate the virtual environment

.. code:: bash

   source ~/livepose_venv/bin/activate

Install OpenCV
~~~~~~~~~~~~~~

In order to use LivePose with GPU support using CUDA/CuDNN packages from the NVIDIA repository, you must compile OpenCV from source.

**Note:** If you are using a virtual environment, make sure your virtual environment is activated before proceeding with this step.

1. Install Dependencies

.. code:: bash

   sudo apt install cmake pkg-config unzip wget
   sudo apt install libavresample-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev
   sudo apt install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
   sudo apt install libgtk2.0-dev libgtk-3-dev
   pip3 install numpy

2. Download OpenCV Source Code You should download these archives somewhere outside of the LivePose directory, in your ``$HOME`` directory or elsewhere. You can ultimately put them wherever you prefer on your system.

.. code:: bash

   cd $HOME
   wget -O opencv.zip https://github.com/Itseez/opencv/archive/4.5.3.zip
   wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/4.5.3.zip
   unzip opencv.zip
   unzip opencv_contrib.zip

3. Compile OpenCV (Note the compilation will take some time)

.. code:: bash

   cd opencv-4.5.3
   mkdir build
   cd build

**Note**: if you're not using a virtual environment, you can ignore the ``-D CMAKE_INSTALL_PREFIX`` line.

.. code:: bash

   cmake \
     -D CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV \
     -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-4.5.3/modules \
     -D BUILD_opencv_python3=ON \
     -D HAVE_opencv_python3=ON \
     -D WITH_CUDA=ON \
     -D CUDA_GENERATION="Auto" \
     -D WITH_CUDNN=ON \
     -D OPENCV_DNN_CUDA=ON \
     -D WITH_CUBLAS=1 \
     ..

   make -j$(nproc)

   sudo make install && sudo ldconfig

4. Make OpenCV discoverable by pip install

``pip install`` will look for ``dist-info`` or ``egg-info`` files to find packages. In the OpenCV source directory:

.. code:: bash

   python3 modules/python/package/setup.py dist_info --egg-base=`pwd`/build
   cp -r opencv.dist-info $VIRTUAL_ENV/lib/python3.8/site-packages/opencv_python.dist-info 

(Optional) Install MMPose backend dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MMPose is recommended for exploring an extensive collection of pose estimation algorithms, including results from the newest research works.

Note before installation: MMPose can work with CPU only but does benefit from a setup where CUDA is available, on NVIDIA GPUs (including Jetson boards).

Follow the instructions from `mmpose <https://github.com/open-mmlab/mmpose/blob/master/docs/en/install.md>`__ to install `torch <https://gitlab.com/sat-mtl/tools/forks/pytorch>`__, `torchvision <https://gitlab.com/sat-mtl/tools/forks/pytorch-vision>`__, `mmcv <https://gitlab.com/sat-mtl/tools/forks/mmcv>`__ and other dependencies.

(Optional) Install Trt backend dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Trt is the fatest deep learning model used for LivePose.

Note before installation: Trt requires NVIDIA GPUs (including Jetson boards).

Follow the instructions from `trt_pose <https://gitlab.com/sat-mtl/tools/forks/trt_pose>`__ to install `torch <https://gitlab.com/sat-mtl/tools/forks/pytorch>`__, `torchvision <https://gitlab.com/sat-mtl/tools/forks/pytorch-vision>`__, `torch2trt <https://gitlab.com/sat-mtl/tools/forks/torch2trt>`__ (Jetcam and other miscellaneous packages are not needed); and `trt_pose <https://gitlab.com/sat-mtl/tools/forks/trt_pose>`__ itself.

(Optional) Install libmapper
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install libmapper with swig for python bindings following instructions from `here <https://github.com/libmapper/libmapper/blob/main/doc/how_to_compile_and_run.md>`__

Install remaining Python dependencies with pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install remaining Python dependencies with pip

.. code:: bash

   export SETUPTOOLS_USE_DISTUTILS=stdlib # ModuleNotFoundError: No module named 'setuptools._distutils'
   pip3 install .

Optional extras
^^^^^^^^^^^^^^^

Optional components (cameras, backends, filters and outputs) can be activated as a semicolon-separated list with environment variable ``LIVEPOSE_EXTRAS``. Examples:

-  ``pip3 install .``: if ``LIVEPOSE_EXTRAS`` is not defined, by default all extras are activated their dependencies searched and required
-  ``LIVEPOSE_EXTRAS="all" pip3 install .``: activates all extras and installs their dependencies (useful if ``LIVEPOSE_EXTRAS`` had already been set to other values)
-  ``LIVEPOSE_EXTRAS="posenet;osc" pip3 install .``: activates and searches dependencies for ``posenet`` and ``osc``, but deactivates all other extras (such as ``libmapper``, ...)

Tip: use option ``-e`` in development mode, so that ``pip3 install -e .`` installs all dependencies but LivePose itself, and thus LivePose source code modifications are active directly at runtime without re-installation.

Note for developers: this command is required to be run everytime after switching between branches that provide different components.

Run the Demo
------------

You can now run LivePose! To try it out with the default settings run the following command from the top- level LivePose directory:

.. code:: bash

   ./livepose.sh

To exit the virtual environment when you are not installing or using LivePose:

.. code:: bash

   deactivate
