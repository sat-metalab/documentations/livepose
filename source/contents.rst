Introduction
============

.. toctree::
   :maxdepth: 1
   :caption: On this site:
   
   Installation
   Usage
   Config
   OSC
   
   Contributing
   News
   
   Back to main page <index>

LivePose is a command line tool which tracks people skeletons from a RGB
or grayscale video feed (live or not), applies various filters on them
(for detection, selection, improving the data, etc) and sends the
results through the network (OSC and Websocket are currently supported).

As some example of what the software can do, there are filters to detect
specific actions (jumping, raising arms), detecting skeletons position
onto a plane, controlling the avatar in `Mozilla
Hubs <https://hubs.mozilla.org>`__, etc. There are some ongoing work to
add support for 3D skeleton detection using multiple cameras, and for
more robust action detection using a neural network.

This software has been tested on Ubuntu 20.04.

Installation
============

Read `Installation <./Installation.html>`__ for installation instructions.

Usage and Configuration
=======================

Read `Usage <./Usage.html>`__ to understand how to launch LivePose.

Read `Config <./Config.html>`__ to understand how to create configuration
files.

Read `OSC <./OSC.html>`__ to discover all Open Sound Control messages sent 
by LivePose.

Contributing and Development
============================

Read `Contributing <./Contributing.html>`__ for development instructions.

Releases
========

Read `Release Notes <./News.html>`__.
