Alternative Installations
=========================

This page contains a collection of instructions for customizing the installation of LivePose and its dependencies.

Note that Ubuntu 22.04 and 20.04 are the only versions of Ubuntu supported for LivePose. There are no installation instructions for older Ubuntu versions.

The alternative installations each have their own page:

-  `Installing Without GPU Support <Installation_no_gpu.html>`__
-  `Compiling Dependencies From Source <Installation_compile_from_source.html>`__
-  `Installing on a Jetson board <Installation_jetson.html>`__
